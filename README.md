# Présentation

Gérer ses documents efficacement avec la programmation lettrée

## Auteur

[Vincent Goulet](https://vgoulet.act.ulaval.ca/), École d'actuariat,
Université Laval

## Résumé

Plusieurs enseignants reconnaitront la situation suivante et les
praticiens pourront sans doute l'adapter à leur contexte. Vous rédigez
des exercices et leurs solutions. Celles-ci comportent du code R dont
la véracité permet de valider à la fois les exercices et les
solutions. Vous devez non seulement conserver tout ce matériel, mais
aussi vous assurer que les questions, les réponses et le code
concordent toujours. J'expliquerai comment j'ai pu résoudre
efficacement ce problème de gestion de code source avec LaTeX et une
utilisation astucieuse de Sweave et Stangle.
