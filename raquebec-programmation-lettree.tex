%%% Copyright (C) 2019 Vincent Goulet
%%%
%%% Ce fichier et tous les fichiers .tex ou .Rnw dont la racine est
%%% mentionnée dans les commandes \include et \input ci-dessous font
%%% partie du projet «Gérer ses documents efficacement avec la
%%% programmation lettrée - R à Québec 2019»
%%% https://gitlab.com/vigou3/raquebec-programmation-lettree
%%%
%%% Cette création est mise à disposition selon le contrat
%%% Attribution-Partage dans les mêmes conditions 4.0
%%% International de Creative Commons.
%%% https://creativecommons.org/licenses/by-sa/4.0/

\documentclass[aspectratio=169,10pt,xcolor=x11names,english,french]{beamer}
% \SweaveUTF8

  \usepackage[noae]{Sweave}
  \usepackage{babel}
  \usepackage[autolanguage]{numprint}
  \usepackage{amsmath}
  \usepackage{currfile}                  % nom fichier de script
  \usepackage{changepage}                % page licence
  \usepackage{tabularx}                  % page licence
  \usepackage{fontawesome}               % icônes
  \usepackage{awesomebox}                % boites signalétiques
  \usepackage{listings}                  % code source
  \usepackage[overlay,absolute]{textpos} % couvertures
  \usepackage{metalogo}                  % logo \XeLaTeX

  %% =============================
  %%  Informations de publication
  %% =============================
  \title{Gérer ses documents efficacement avec la programmation lettrée}
  \author{Vincent Goulet}
  \renewcommand{\year}{2019}
  \renewcommand{\month}{05-1}
  \newcommand{\reposurl}{https://gitlab.com/vigou3/raquebec-programmation-lettree}

  %% =======================
  %%  Apparence du document
  %% =======================

  %% Thème Beamer
  \usetheme{metropolis}

  %% Mathématiques en arev et ajustement de la taille des autres
  %% polices Fira; https://tex.stackexchange.com/a/405211/24355
  \usepackage{arevmath}
  \setsansfont[%
    BoldFont = {Fira Sans SemiBold},
    ItalicFont = {Fira Sans Book Italic},
    BoldItalicFont = {Fira Sans SemiBold Italic}]{Fira Sans Book}

  %% Couleurs additionnelles
  \definecolor{comments}{rgb}{0.7,0,0}  % rouge foncé
  \definecolor{link}{rgb}{0,0.4,0.6}    % liens internes
  \definecolor{url}{rgb}{0.6,0,0}       % liens externes
  \definecolor{rouge}{rgb}{0.85,0,0.07} % bandeau rouge UL
  \definecolor{or}{rgb}{1,0.8,0}        % bandeau or UL
  \colorlet{alert}{mLightBrown} % alias pour couleur Metropolis
  \colorlet{dark}{mDarkTeal}    % alias pour couleur Metropolis
  \colorlet{code}{mLightGreen}  % alias pour couleur Metropolis

  %% Hyperliens
  \hypersetup{%
    pdfauthor = {Vincent Goulet},
    pdftitle = {Introduction à la science des données},
    colorlinks = {true},
    linktocpage = {true},
    allcolors = {link},
    urlcolor = {url},
    pdfpagemode = {UseOutlines},
    pdfstartview = {Fit},
    bookmarksopen = {true},
    bookmarksnumbered = {true},
    bookmarksdepth = {subsection}}

  %% Paramétrage de babel pour les guillemets
  \frenchbsetup{og=«, fg=»}

  %% Sections de code source
  \lstloadlanguages{[LaTeX]TeX}
  \lstset{language=[LaTeX]TeX,
    basicstyle=\small\ttfamily\NoAutoSpacing,
    commentstyle=\color{comments}\slshape,
    keywordstyle=\mdseries,
    emphstyle=\color{alert}\bfseries,
    escapeinside=`',
    extendedchars=true,
    showstringspaces=false,
    backgroundcolor=\color{LightYellow1},
    frame=leftline,
    framerule=2pt,
    framesep=5pt,
    xleftmargin=7.4pt
  }

  %% =====================
  %%  Nouvelles commandes
  %% =====================

  %% Lien externe
  \newcommand{\link}[2]{\href{#1}{#2~\raisebox{-0.2ex}{\faExternalLink}}}

  %% Renvoi vers GitLab sur la page de copyright
  \newcommand{\viewsource}[1]{%
    \href{#1}{\faGitlab\ Voir sur GitLab}}

  %% Simili commande \HUGE
  \newcommand{\HUGE}{\fontsize{36}{36}\selectfont}

  %% Boite signalétique additionnelle (basée sur awesomebox.sty) pour
  %% renvoi vers fichier
  \newcommand{\gotobox}[1]{%
    \awesomebox{\faMapSigns}{\aweboxrulewidth}{black}{#1}}

  %% Raccourcis usuels vg
  \newcommand{\code}[1]{\textcolor{code}{\texttt{#1}}}

  %%% =======
  %%%  Varia
  %%% =======

  %% Longueurs pour la composition des pages couvertures avant et
  %% arrière
  \newlength{\banderougewidth} \newlength{\banderougeheight}
  \newlength{\bandeorwidth}    \newlength{\bandeorheight}
  \newlength{\imageheight}     \newlength{\imagewidth}
  \newlength{\logoheight}

  %% Nom du fichier de script associé
  \def\scriptfilename{\currfilebase.R}

\begin{document}

%% frontmatter
\input{couverture-avant}
\input{notices}

\begin{frame}[plain]
  \TPGrid{160}{90}
  \begin{textblock*}{128\TPHorizModule}(16.2\TPHorizModule,10\TPVertModule)
    \includegraphics[width=127.6\TPHorizModule,keepaspectratio]{%
      images/real_programmers} \\
    \footnotesize\sffamily\flushleft\vspace{-\baselineskip}%
    Tiré de \href{https://xkcd.com/378/}{XKCD.com}
  \end{textblock*}
\end{frame}

\begin{frame}[standout]
  \begin{minipage}{0.52\linewidth}
    \normalsize
    \begin{quote}
      \selectlanguage{english}
      I believe that the time is ripe for significantly better
      documentation of programs, and that we can best achieve this by
      considering programs to be works of literature. Hence, my title:
      \emph{Literate Programming}. \par
      \hfill%
      --- Donald Knuth, \alert{1982}
    \end{quote}
  \end{minipage}
  \hfill
  \begin{minipage}{0.47\linewidth}
    \includegraphics[width=\linewidth,keepaspectratio]{images/knuth}
  \end{minipage}
\end{frame}

\begin{frame}[plain]
  Ce qui a le plus contribué à changer mes méthodes de travail depuis
  20 ans
  \begin{itemize}
  \item S-PLUS et R
  \item Gestion de versions
  \item Sweave et la programmation lettrée
  \end{itemize}
\end{frame}

%% mainmatter
\begin{frame}
  \frametitle{Programmation lettrée}

  Une (autre) idée de Donald Knuth lorsqu'il a créé {\TeX} pour
  rédiger \emph{The Art of Computer Programming}.
  \begin{itemize}
  \item Combiner un programme et sa documentation dans un même
    fichier dans un format facile à consulter pour un \alert{humain}
    \begin{center}
      \includegraphics[width=\linewidth]{images/cweave.jpg}
    \end{center}
  \item Plusieurs systèmes au fil du temps:
    \link{https://fr.wikipedia.org/wiki/WEB}{WEB} (Knuth, 1984),
    \link{https://en.wikipedia.org/wiki/CWEB}{CWEB} (Knuth et Levy, 1987),
    \link{https://en.wikipedia.org/wiki/Noweb}{noweb} (Ramsey, 1989),
    \link{https://en.wikipedia.org/wiki/Sweave}{Sweave} (Leisch, 2002),
    \link{https://en.wikipedia.org/wiki/Knitr}{knitr} (Xie, 2012)
  \end{itemize}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Exemple de document combinant {\LaTeX} et code R}

  \code{foo.Rnw}
\begin{lstlisting}[emph={Sexpr,@}]
L'utilisateur de R interagit avec l'interprète en entrant
des commandes à l'invite de commande:
`\alert{<<echo=TRUE>>=}'
2 + 3
@
La commande \verb=exp(1)= donne \Sexpr{exp(1)},
la valeur du nombre $e$.
\end{lstlisting}
\end{frame}

\begin{frame}[fragile=singleslide]
  \frametitle{Après traitement par Sweave dans R}

  \code{Sweave("foo.Rnw")}
  $\rightarrow$ \code{foo.tex}
\begin{lstlisting}[emph={Schunk,Sinput,Soutput}]
L'utilisateur de R interagit avec l'interprète en entrant
des commandes à l'invite de commande:
\begin{Schunk}
\begin{Sinput}
> 2 + 3
\end{Sinput}
\begin{Soutput}
[1] 5
\end{Soutput}
\end{Schunk}
La commande \verb=exp(1)= donne 2.71828182845905,
la valeur du nombre $e$.
\end{lstlisting}
\end{frame}

\begin{frame}
  \frametitle{Processus en un coup d'oeil}

  \begin{minipage}[t]{0.15\textwidth}
    \centering
    {\Huge\faFileTextO} \\
    \bigskip
    \code{foo.Rnw}
  \end{minipage}
  \hfill{\LARGE\faArrowRight}\hfill
  \begin{minipage}[t]{0.15\textwidth}
    \centering
    {\Huge\faCogs} \\
    \bigskip
    \code{Sweave}
  \end{minipage}
  \hfill{\LARGE\faArrowRight}\hfill
  \begin{minipage}[t]{0.15\textwidth}
    \centering
    {\Huge\faFileTextO} \\
    \bigskip
    \code{foo.tex}
  \end{minipage}
  \hfill{\LARGE\faArrowRight}\hfill
  \begin{minipage}[t]{0.15\textwidth}
    \centering
    {\Huge\faCogs} \\
    \bigskip
    \code{latex}
  \end{minipage}
  \hfill{\LARGE\faArrowRight}\hfill
  \begin{minipage}[t]{0.15\textwidth}
    \centering
    {\Huge\faFilePdfO} \\
    \bigskip
    \code{foo.pdf}
  \end{minipage}
\end{frame}

\begin{frame}
  \frametitle{R~Markdown: même idée}
  \begin{itemize}
  \item Texte en format Markdown plutôt que {\LaTeX}
  \item Traitement avec knitr plutôt qu'avec Sweave
  \item Sortie dans plusieurs formats différents, dont HTML
  \end{itemize}

  \bigskip
  \begin{minipage}[t]{0.1\textwidth}
    \centering
    {\huge\faFileTextO} \\
    \bigskip
    \footnotesize\code{foo.Rmd}
  \end{minipage}
  \hfill{\Large\faArrowRight}\hfill
  \begin{minipage}[t]{0.1\textwidth}
    \centering
    {\huge\faCogs} \\
    \bigskip
    \footnotesize\code{knitr}
  \end{minipage}
  \hfill{\Large\faArrowRight}\hfill
  \begin{minipage}[t]{0.1\textwidth}
    \centering
    {\huge\faFileTextO} \\
    \bigskip
    \footnotesize\code{foo.md}
  \end{minipage}
  \hfill{\Large\faArrowRight}\hfill
  \begin{minipage}[t]{0.1\textwidth}
    \centering
    {\huge\faCogs} \\
    \bigskip
    \footnotesize\code{pandoc}
  \end{minipage}
  \hfill
  \parbox{0.04\textwidth}{
    \rotatebox{45}{\Large\faArrowRight} \\
    \rotatebox{-45}{\Large\faArrowRight}}
  \hfill
  \begin{minipage}{0.43\textwidth}
    \begin{minipage}[t]{0.2\linewidth}
      \centering
      {\LARGE\faFileTextO} \\
      \medskip
      \footnotesize\code{foo.tex}
    \end{minipage}
    \;{\large\faArrowRight}\;
    \begin{minipage}[t]{0.2\linewidth}
      \centering
      {\LARGE\faCogs} \\
      \medskip
      \footnotesize\code{latex}
    \end{minipage}
    \;{\large\faArrowRight}\;
    \begin{minipage}[t]{0.2\linewidth}
      \centering
      {\LARGE\faFilePdfO} \\
      \medskip
      \footnotesize\code{foo.pdf}
    \end{minipage} \\
    \phantom{\LARGE\faFileO} \\
    \begin{minipage}[t]{0.2\linewidth}
      \centering
      {\LARGE\faFileCodeO} \\
      \footnotesize\code{foo.html}
    \end{minipage}
    \;{\large\faArrowRight}\;
    \begin{minipage}[t]{0.2\linewidth}
      \centering
      {\LARGE\faGlobe} \\
      \footnotesize Web
    \end{minipage}
    \hspace*{\fill}
  \end{minipage}
\end{frame}

\begin{frame}[standout]
  Nous pouvons faire mieux avec la programmation lettrée
\end{frame}

\begin{frame}
  \frametitle{Premier exemple}

  \textbf{Atelier d'introduction à la science des données}
  (\link{https://gitlab.com/vigou3/atelier-science-donnees}{code
    source})

  \bigskip
  \begin{minipage}[t]{0.48\linewidth}
    \centering
    Matériel pédagogique

    \bigskip
    \begin{minipage}{0.4\linewidth}
      \centering
      {\HUGE\faFilePdfO}

      \medskip diapositives
    \end{minipage}
    \begin{minipage}{0.4\linewidth}
      \centering
      {\HUGE\faFileTextO}

      \medskip fichier de script
    \end{minipage}
  \end{minipage}
  \hfill
  \pause
  \begin{minipage}[t]{0.48\linewidth}
    \centering
    Source

    \bigskip
    {\HUGE\faFileText}

    \medskip fichier unifié
    \begin{center}
      \alert{\faLightbulbO} gestion facilitée \\
      \alert{\faLightbulbO} validation du script
    \end{center}
  \end{minipage}
\end{frame}

\begin{frame}[plain]
  \gotobox{Étudiez le fichier \code{exemple-diapos.Rnw} livré avec ces
    diapositives. Il fournit la structure de base d'un fichier
    combinant le texte de diapositives et le code destiné à être
    distribué séparément dans un fichier de script.}
\end{frame}

\begin{frame}
  \frametitle{Second exemple}

  \textbf{Travail pratique de programmation en R}

  \bigskip
  \begin{minipage}[t]{0.54\linewidth}
    \centering
    Matériel à gérer

    \bigskip
    \begin{minipage}{0.3\linewidth}
      \centering
      {\HUGE\faFilePdfO}

      \medskip énoncé
    \end{minipage}
    \hfill
    \begin{minipage}{0.3\linewidth}
      \centering
      {\HUGE\faFileTextO}

      \medskip solutions
    \end{minipage}
    \hfill
    \begin{minipage}{0.3\linewidth}
      \centering
      {\HUGE\faFileTextO}

      \medskip tests unitaires
    \end{minipage}
  \end{minipage}
  \hfill
  \pause
  \begin{minipage}[t]{0.42\linewidth}
    \centering
    Source

    \bigskip
    {\HUGE\faFileText}

    \medskip fichier unifié
    \begin{center}
    \alert{\faLightbulbO} proximité entre questions \\ et solutions \\
    \alert{\faLightbulbO} tests exécutés sur solutions
    \end{center}
  \end{minipage}
\end{frame}

\begin{frame}[plain]
  \gotobox{Étudiez le fichier \code{exemple-travail.Rnw} livré avec
    ces diapositives. Il fournit la structure de base d'un fichier
    combinant l'énoncé d'un travail de programmation, les solutions et
    les tests unitaires.}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Astuce}

  \begin{itemize}[<+->]
  \item Sweave exécute tout le code R d'un fichier \code{.Rnw}
  \item Stangle extrait tout le code R d'un fichier \code{.Rnw}
  \item Combiner les deux et exécuter Stangle \alert{à l'intérieur} de
    Sweave!
\begin{lstlisting}[language=R,emph={Stangle}]
<<echo=FALSE>>=
## nom du présent fichier avec et sans l'extension
file <- "foo.Rnw"
prefix <- paste0(sub(".Rnw", "", file), "-")

## extraction des scripts
Stangle(file, annotate = FALSE, split = TRUE)

## changement du nom des fichiers de script
scripts <- list.files(pattern = paste0("^", prefix, "[^0-9].*\\.R$"))
file.rename(scripts, sub(prefix, "", scripts))
@
\end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Limitations et inconvénients}

  \begin{itemize}
  \item Il ne faut pas oublier de nommer les blocs de code
    (\emph{chunks})
  \item Compliqué d'éviter que du code ne soit \alert{pas} exécuté \\
    (non, \code{eval = FALSE} n'est pas la solution)
  \item Pas fameux pour des applications Shiny \\
    (justement à cause de la puce précédente)
  \item Stangle laisse trainer des fichiers de type \code{foo-001.R}
  \end{itemize}
\end{frame}

%% backmatter
\input{colophon}
\input{couverture-arriere}

\end{document}

%%% Local Variables:
%%% mode: noweb
%%% TeX-engine: xetex
%%% coding: utf-8
%%% End:
